#!/bin/bash

# Author: Orestis Papadopoulos
# Name: venc
# (Simple) Usage: venc <filename>
# Description: Serves as a simple mencoder front-end and video management tool for camera videos
# Last changed: 22/3/2014

BATCH=0

while getopts ":ba" opt; do
  case $opt in
    a)
      echo "-a was triggered, Parameter: $OPTARG" >&2
      ;;
    b)
      echo "for the whole directory" >&2
      BATCH=1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    *)
      echo "call --help" >&2
      exit 1
      ;;
  esac
done

OUTPUTDIR="encodes"
if [ ! -d "$OUTPUTDIR" ]
  then
    mkdir $OUTPUTDIR
fi

LOGFILE="encode.log"
if [ -a $LOGFILE ]; then rm $LOGFILE; fi

if [ $BATCH -eq 1 ]
  then
    for file in *
    do
        output_path="$OUTPUTDIR/${file%.*}.avi" 
        if [ ! -a "$output_path" ]
          then
            mencoder -profile canon2x264 "$file" -o "$output_path" >> "${file%.*}_"encode.log
        else
            echo "$output_path already exists;"
        fi
    done
fi
